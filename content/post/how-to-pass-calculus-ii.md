---
title: How to Pass Calculus II
date: 2018-08-19 17:28:00 +0000
tags:
- math
body: Test
draft: true

---
# How to Pass Calculus II

To avoid becoming a rant, I'm going to focus on describing the approach that worked for me.

## Previous Exams are Your Bible

The absolute best way to prepare for exams is to study exams from previous years. Ideally, the previous exams are written by your current professor and only a year or two old. Exams from other professors at your school are an acceptable alternative.

**Develop an algorithm for solving every question of an exam.** All questions can be broken down into a general series of steps. These steps may require certain techniques (e.g. integration by parts, partial fraction expansion, alternating series test of convergence...) or formulas (exponential model of population growth, tangent plane equation, Taylor series...). Unfortunately, these techniques and formulas must be memorized.

Sometimes the "algorithm" is simple and obvious.

> The length of the arc \\( y = x^2 - \\frac{\\ln{x}}{8} \\), \\( 1 \\le x \\le 2 \\) is
>
> A) \\(3\\)
>
> B) \\( 3 + \\frac{\\ln{2}}{8} \\)
>
> C) \\( 3 - \\frac{\\ln{2}}{8} \\)
>
> D) \\(4\\)
>
> E) \\( 4 - \\frac{\\ln{2}}{8} \\)
>
> F) \\( 4 + \\frac{\\ln{2}}{4} \\)

In this case, just apply the arc length formula:

$$ \\sqrt{1 + (y')^2} $$

Other times the algorithm is more involved.

> Suppose Euler's method with step size \\(h=0.1\\) is used to estimate \\(y(0.2)\\), where \\(y(x)\\) is the solution to the initial-value problem \\(y' = 2xy-y^2, y(0) = 1\\). Which one of the following values is closest to the result that you obtained?
>
> A) 0.900
>
> B) 0.854
>
> C) 0.837
>
> D) 0.823
>
> E) 0.818
>
> F) 0.792

The steps here are something like:

1. Create a table of values with columns: \\(n\\), \\(x_n\\), \\(y_n\\), \\(y_n^'\\)
2. Let \\( \\Delta x = 0.1 \\)
3. Fill in \\( x \\) column using the formula: \\( x_{n+1} = x_n + \\Delta x \\). The final \\(x\\) value is \\(0.2\\).
4. Calculate the first \\(y'\\) (to finish the first row)
5. For all remaining rows: calculate \\(y_{n+1} = y_n + y_n^' \\Delta x\\) and \\(y'\\)

Once you've figured out the steps needed to solve a particular class of problem, **grind through a bunch of practice questions**. This ensures your algorithm is sufficiently general and helps with remembering it. With enough practice, solving exam questions become an exercise in muscle memory rather than thought.

Understanding why a particular series of steps yields a correct answer is relatively unimportant. There are no proofs and you will never be asked to explain your methods. The only thing that matters is correct computation.

## Khan is a Better Teacher

Perhaps it was bad luck, but Khan Academy has been a better calculus teacher than any professor I've ever had. 

University professors tend to choose terrible, overly complex examples that muddy the waters, making it more difficult to understand the concept supposedly being illustrated.

One professor always used \\(m\\) and \\(n\\) for variables. Their handwriting made it nearly impossible to tell the two apart, occasionally she even confused herself.

The problem sets on Khan Academy tend to be more focused. It is also easy to repeat problem sets. The calculus textbook I was assigned often had nearly 100 questions at the end of each chapter. Choosing a representative sample was difficult.

## Sympy makes studying concepts easier

Part of the annoyance of studying Calculus is the long, tedious calculations. If you want to explore a new idea, I recommend installing Anaconda and working in a Jupyter notebook. Sympy is a Python library for symbolic math which makes it easy to compute integrals, derivatives, limits, series...etc. 

## Practice, practice, practice

Boiling it down, the strategy for solving calculus questions is simple:

1. Understand the problem on a basic level
2. Figure out a series of steps to solve it
3. Grind through dozens of similar questions until the steps have been refined and memorized

That's it.